import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;
 
public class ClienteCalculadora {

  public static void main(String[] args) {
    try {

        // Localiza o registry. É possível usar endereço/IP porta
       Registry registry = LocateRegistry.getRegistry("127.0.1.1");
       // Consulta o registry e obtém o stub para o objeto remoto
       Calculadora calc = (Calculadora) registry
           .lookup("calculadora");
       // A partir deste momento, cahamadas à Caluladora podem ser
       // feitas como qualquer chamada a métodos
       
       
    Scanner input = new Scanner(System.in);
    boolean mainLoop = true;

    int choice = -1;
    
         while(choice != 0) {
            System.out.println("Menu da Calculadora\n");
            System.out.print("1.) Adição \n");
            System.out.print("2.) Subtração.\n");
            System.out.print("3.) Multiplicação.\n");
            System.out.print("4.) Divisão.\n");
            System.out.print("5.) Gerar um numero aleatorio.\n");
            System.out.print("6.) Fatorial.\n");
            System.out.print("7.) Metros para Quilometros.\n");
            System.out.print("8.) Raiz quadrada.\n");
            System.out.print("9.) Ao quadrado.\n");
            System.out.print("10.) Ao cubo.\n");
            System.out.print("11.) Converter Celsius para Fahrenheit.\n");
            System.out.print("12.) Raiz cubica.\n");
            System.out.print("13.) Exponencial.\n");
            System.out.print("15.) Sair\n");
            System.out.print("\nEscolha uma operação: ");

            choice = input.nextInt();
             
            switch(choice){
                case 1:
                    //Soma
                    Numero adNumf, adNuml, sum;
                    System.out.print("Digite o primeiro numero: ");
                    adNumf = new NumeroImpl(input.nextInt());
                    System.out.print("\nDigite o segundo numero: ");
                    adNuml = new NumeroImpl(input.nextInt());
                    sum = calc.soma(adNumf,  adNuml);
                    System.out.print("A soma desses numeros é: " + sum.getValor() + "\n");
                    break;
                          
                          
                case 2:
                    //Subtração
                    Numero subNum1, subNum2, sum2;
                    System.out.println("\nDigite o primeiro numero: ");
                    subNum1 = new NumeroImpl(input.nextInt());
                    System.out.println("Digite o segundo numero: ");
                    subNum2 = new NumeroImpl(input.nextInt());
                    sum2 = calc.subtrai(subNum1 , subNum2);
                    System.out.println("A subtração desses numeros é: " + sum2.getValor()+ "\n");
                    break;

                case 3:
                    //Multiplicação
                    Numero multNum1, multNum2, multTotal;
                    System.out.println("Digite o primeiro numero: ");
                    multNum1 = new NumeroImpl(input.nextInt());
                    System.out.println("Digite o segundo numero: ");
                    multNum2 = new NumeroImpl(input.nextInt());
                    multTotal = calc.multiplica(multNum1 , multNum2);
                    System.out.println("A multiplicação desses numeros é: " +multTotal.getValor()+ "\n");
                    break;

                case 4: 
                    //Divisão
                    Numero divNum1, divNum2, divTotal;
                    System.out.println("Digite o numerador: ");
                    divNum1 = new NumeroImpl(input.nextInt());
                    System.out.println("Digite o denomidanor: ");
                    divNum2 = new NumeroImpl(input.nextInt());

                    try {
                        divTotal = calc.divide(divNum1, divNum2);
                        System.out.println("O resultado da divisão é: " + divTotal.getValor()+ "\n");
                    } catch (DivisaoPorZeroException e) {
                        System.out.println("Não é possível dividir por zero. Selecione um novo denominador: ");
                        divNum2 = new NumeroImpl(input.nextInt());

                        try {
                            divTotal = calc.divide(divNum1, divNum2);
                            System.out.println("O resultado da divisão com o novo denominador é: " + divTotal.getValor());
                        } catch (DivisaoPorZeroException ex) {
                            System.out.println("Não é possível dividir por zero novamente. Voltando para o Menu.");
                        }
                    }
                    break;

                case 5:
                    //Gerar numero aleatorio
                    Numero limL, limH, rand;
                    System.out.println("Digite seu limite inferior: ");
                    limL = new NumeroImpl(input.nextInt());
                    System.out.println("Digite seu limite máximo: ");
                    limH = new NumeroImpl(input.nextInt());
                    rand = calc.gera(limL,limH);
                    System.out.println("Dados seus limites, o número aleatório será: " +rand.getValor()+ "\n");
                    break;

                case 6:
                    //Fatorial
                    Numero num, fatNum;
                    System.out.println("Digite o numero para calcular o fatorial: ");
                    num = new NumeroImpl(input.nextInt());
                    fatNum= calc.fatorial(num);
                    System.out.println("O fatorial é: " +fatNum.getValor());
                    break;

                case 7:
                    //Metro para Quilometros
                    Numero numM, numKm;
                    System.out.println("Digite o comprimento em metros: ");
                    numM = new NumeroImpl(input.nextInt());
                    numKm= calc.metroKm(numM);
                    System.out.println("Comprimento em quilometros: " +numKm.getValor());
                    break;

                case 8:
                    //Raiz Quadrada
                    Numero numRaiz, resulRaiz;
                    System.out.println("Digite um numero: ");
                    numRaiz = new NumeroImpl(input.nextInt());
                    resulRaiz= calc.raizQuadrada(numRaiz);
                    System.out.println("A raiz quadrada é: " +resulRaiz.getValor());
                    break;

                case 9:
                    //Ao Quadrado
                    Numero numQua, resultQua;
                    System.out.println("Digite um numero: ");
                    numQua = new NumeroImpl(input.nextInt());
                    resultQua= calc.quadrado(numQua);
                    System.out.println("Numero ao quadrado é: " +resultQua.getValor());
                    break;

                case 10:
                    //Ao cubo
                    Numero numCu, resulCu;
                    System.out.println("Digite um numero: ");
                    numCu = new NumeroImpl(input.nextInt());
                    resulCu= calc.cubo(numCu);
                    System.out.println("Resultado ao cubico é: " +resulCu.getValor());
                    break;

                case 11:
                    //Celsius para Fahrenheit
                    Numero tempC, tempF;
                    System.out.println("Digite a temperatura em Celsius: ");
                    tempC = new NumeroImpl(input.nextInt());
                    tempF= calc.celsiusF(tempC);
                    System.out.println("A temperatura em Fahrenheit é: " +tempF.getValor());
                    break;

                case 12:
                    //Raiz Cubica
                    Numero numRaCu, resultRaCu;
                    System.out.println("Digite um numero: ");
                    numRaCu = new NumeroImpl(input.nextInt());
                    resultRaCu= calc.raizCubica(numRaCu);
                    System.out.println("A raiz cubica é: " +resultRaCu.getValor());
                    break;

                case 13:
                    //Exponencial
                    Numero numExp, resultExp;
                    System.out.println("Digite um numero: ");
                    numExp = new NumeroImpl(input.nextInt());
                    resultExp= calc.exponencial(numExp);
                    System.out.println("A raiz cubica é: " +resultExp.getValor());
                    break;



                case 15:
                    System.out.println("Saindo do programa...");
                    System.exit(0);
                     break;

                default :
                    System.out.println("Esta não é uma opção de menu válida! Selecione outra");
                    break;
                
            }

            
            }
     



    } catch (Exception e) {
       System.err.println("Ocorreu um erro no cliente: " +
                          e.toString());
    }
  }
}
