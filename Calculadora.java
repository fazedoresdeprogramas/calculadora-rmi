import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Calculadora extends Remote {
    public Numero soma (Numero a, Numero b)
        throws RemoteException;

    public Numero subtrai (Numero a, Numero b)
        throws RemoteException;

    public Numero multiplica (Numero a, Numero b)
        throws RemoteException;

    public Numero divide (Numero a, Numero b)
        throws RemoteException, DivisaoPorZeroException;

    public Numero gera (Numero a, Numero b)
            throws RemoteException;

    public Numero fatorial(Numero a)
        throws RemoteException;

    public Numero metroKm(Numero a)
        throws RemoteException;

    public Numero raizQuadrada(Numero a)
        throws RemoteException;

    public Numero quadrado(Numero a)
        throws RemoteException;

    public Numero cubo(Numero a)
        throws RemoteException;

    public Numero celsiusF(Numero a)
        throws RemoteException;

    public Numero raizCubica(Numero a)
        throws RemoteException;

    public Numero exponencial(Numero a)
        throws RemoteException;



 }
