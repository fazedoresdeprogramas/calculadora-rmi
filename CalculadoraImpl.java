import java.util.Random;

public class CalculadoraImpl implements Calculadora {

    public Numero soma(Numero a, Numero b) {
        return new NumeroImpl(a.getValor() + b.getValor());
    }

    public Numero subtrai(Numero a, Numero b) {
        return new NumeroImpl(a.getValor() - b.getValor());
    }

    public Numero multiplica(Numero a, Numero b) {
        return new NumeroImpl(a.getValor() * b.getValor());
    }

    public Numero divide(Numero a, Numero b)
            throws DivisaoPorZeroException {
        if (b.getValor() == 0) throw new DivisaoPorZeroException();
        return new NumeroImpl(a.getValor() / b.getValor());
    }

    public Numero gera(Numero a, Numero b) {
        Random random = new Random();
        double min = a.getValor();
        double max = b.getValor();

        double numeroAleatorio = random.nextDouble() * (max - min) + min;
        return new NumeroImpl(numeroAleatorio);

    }

    public Numero fatorial(Numero a){
        int fatorial = 1;
        for(int i= 1; i<= a.getValor(); i++){
            fatorial *= i;
        }

        return new NumeroImpl(fatorial);
    }

    public Numero metroKm(Numero a){
        double km = a.getValor()/1000.0;
        return new NumeroImpl(km);
    }

    public Numero raizQuadrada(Numero a){
        double raizQua = Math.sqrt(a.getValor());
        return new NumeroImpl(raizQua);
    }

    public Numero quadrado(Numero a){
        double quadrado = a.getValor() * a.getValor();
        return new NumeroImpl(quadrado);
    }

    public Numero cubo(Numero a){
        double cubo = a.getValor() * a.getValor() * a.getValor();
        return new NumeroImpl(cubo);
    }

    public Numero celsiusF(Numero a){
        double tempF = (a.getValor() * 9/5)+32;
        return new NumeroImpl(tempF);
    }

    public Numero raizCubica(Numero a){
        double raizCu = Math.sqrt(a.getValor());
        return new NumeroImpl(raizCu);
    }

    public Numero exponencial(Numero a){
        double exponencial = Math.exp(a.getValor());
        return new NumeroImpl(exponencial);
    }



}
